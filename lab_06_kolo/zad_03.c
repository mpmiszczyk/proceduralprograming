#include <stdio.h>
#include <stdlib.h>
#define PRIME_MAX 100000
#define PRIME 0
#define NOT_PRIME 1

void findPrimesSmallerThen(int primeNumber,int *primeArr, int *primeFound);

void savePrimeToArr(int prime,int  primeArr[],int *primeFound);

void printArr(const int *arr, int arrSize); 
 
int main (void){

	int primeNumber;
	int primeFound = 0;
	int *primeArr;
	
	primeArr = (int*)malloc(sizeof(int));

	printf ("prosze podac przedzial w jakim mam szukac liczb pierwszysch\n");

	do {
		printf ("liczba musi byc mniejsza od %d\n", PRIME_MAX);
		scanf(" %d", &primeNumber);
	}while (!(( 0 <= primeNumber) && (primeNumber <= PRIME_MAX)));

	
	printf("29\n");
	findPrimesSmallerThen(primeNumber, primeArr, &primeFound);

	printArr(primeArr,primeFound);

	free(primeArr);


	return 0;
	
}




void findPrimesSmallerThen(int primeNumber,int *primeArr, int *primeFound){

	int * numbers;

	primeNumber++; //inkrementacja na potrzeby lepszej czytelności dalszego kodu
	numbers = (int*)malloc(primeNumber * sizeof(int));
	
	for (int i = 0; i < primeNumber ; ++i){
		numbers[i]=PRIME;
	}

	
	for (int i = 2; i < primeNumber ; ++i){
		if (numbers[i] == PRIME){
			savePrimeToArr( i ,primeArr, primeFound);

			for (int k = 1; k*i < primeNumber; ++k){
				numbers[k*i] = NOT_PRIME;
			}
			
		}

	}
	printf("primeFound = %d\n", *primeFound );

	free(numbers);
}



void savePrimeToArr(int prime,int  primeArr[],int *primeFound){
	++(*primeFound);
	
	primeArr = realloc(primeArr,*primeFound * sizeof(int));
	if (primeArr == NULL){
		printf("błąd przypisania pamięci do tablicy");
		exit(EXIT_FAILURE);
	}
	
	primeArr[*primeFound-1] = prime;
	
}


void printArr(const int *arr,int arrSize){
	for (int i =0; i < arrSize; ++i){
		printf("arr[%3d] = %5d\n",i,arr[i]);
	}
}




