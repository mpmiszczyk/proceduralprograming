#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#define MAX_WORD 50

int isLetter (const char cr);

int getWordFromFile(char * pWord, FILE * file);
int stripFromPunct(char *pWord);
int rymes (char * pch_word1,char * pch_word2);

int main (void ){
	
	char word1[MAX_WORD];	
	char word2[MAX_WORD];	
	char fileName[MAX_WORD];
	FILE * file;
	int rymeCount = 0;
		

	printf ("prosze podać słowo do porównania; ");
	do { printf ("max długość: %d\n",MAX_WORD);
	} while ( fscanf (stdin," %s",word1) == EOF);

	printf ("prosze podać nazwę pliku do wyszukania rymów; ");
	do { printf ("max długość: %d\n",MAX_WORD);
	} while ( fscanf (stdin," %s",fileName) == EOF);

	if ((file = fopen(fileName,"r")) == NULL){
		fprintf(stderr, "błąd prz otwieraniu pliku %s; plik moźe nie istnieć, lub możesz nie mieć do niego uprawnien\n",fileName);
		exit(-1);
	}

	while(getWordFromFile(word2,file)){
		if (rymes(word1,word2)){
			puts(word2);
			++rymeCount;
		}
		
	}

	printf ("zliczono %d rymów\n",rymeCount);

	return 0;
}


int getWordFromFile(char * pWord, FILE * file){
	if (fscanf(file," %s",pWord) == EOF){
		return 0;
	}else{
		stripFromPunct(pWord);
		return 1;
	}
}

int isLetter (const char cr){
	return (isprint(cr) && !(isdigit(cr) || ispunct(cr)));
}	
int stripFromPunct(char *pWord){
	int i=0;
	int k=0;

	while (pWord[i] != '\0' ){
		if (!ispunct(pWord[i])){
			pWord[k]=pWord[i];
			++k;
		}
		++i;
	}
	pWord[k]='\0';
	return k;
}

int rymes (char * pch_word1,char * pch_word2){
	int len1 = strlen(pch_word1);
	int len2 = strlen(pch_word2);
	
	for (int i=1; i<=3 && len1-i>=0 && len2-i>=0; ++i){
		
		if (pch_word1[len1-i] != pch_word2[len2-i]){//TODO porównywanie niezalerzne od wielkości liter
			return 0;
			printf ("not ryme \n");
		}
	}
	
	
	return 1;

}
