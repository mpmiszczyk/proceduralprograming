#include <stdio.h>
#include <stdlib.h>
#define PRIME_MAX 10000000
#define PRIME 0
#define NOT_PRIME 1

void findPrimesSmallerThen(int primeNumber,int **pPrimeArr, int *pPrimeFound);

void savePrimeToArr(int prime,int  **pPrimeArr,int *pPrimeFound);

void printArr(const int *arr, int arrSize); 
 
int main (void){

	int primeNumber;
	int primeFound = 0;
	int *primeArr = NULL;
	

	printf ("prosze podac przedzial w jakim mam szukac liczb pierwszysch\n");
	do {
		printf ("liczba musi byc mniejsza od %d\n", PRIME_MAX);
		scanf(" %d", &primeNumber);
	}while (!( 0 <= primeNumber && primeNumber <= PRIME_MAX));


	findPrimesSmallerThen(primeNumber, &primeArr, &primeFound);
	printf("primeFound %d\n",primeFound);

	printArr(primeArr,primeFound);

	free(primeArr);

	return 0;
	
}




void findPrimesSmallerThen(int primeNumber,int **pPrimeArr, int *pPrimeFound){

	int * numbers;

	primeNumber++; //inkrementacja na potrzeby lepszej czytelności dalszego kodu
	numbers = (int*)malloc(primeNumber * sizeof(int));
	if (numbers == NULL){
		fprintf(stderr,"błąd przypisania pamięci do tablicy numbers");
		free(numbers);
		exit(EXIT_FAILURE);
	}

	
	for (int i = 0; i < primeNumber ; ++i){
		numbers[i]=PRIME;
	}
	
	for (int i = 2; i < primeNumber ; ++i){
		if (numbers[i] == PRIME){
			savePrimeToArr( i ,pPrimeArr, pPrimeFound);
			for (int k = 1; k*i < primeNumber; ++k){
				numbers[k*i] = NOT_PRIME;
			}
		}

	}

	free(numbers);
}



void savePrimeToArr(int prime,int **pPrimeArr,int *pPrimeFound){
	int *pNewArr;
	
	++(*pPrimeFound);
	
	pNewArr = (int*)realloc(*pPrimeArr, (*pPrimeFound) * sizeof(int));
	if (pNewArr == NULL){
		fprintf(stderr,"błąd przypisania pamięci do tablicy");
		free(*pPrimeArr);
		exit(EXIT_FAILURE);
	}
	*pPrimeArr = pNewArr;

	(*pPrimeArr)[(*pPrimeFound)-1] = prime;
}


void printArr(const int *arr, int  arrSize){
	for (int i = 0; i < arrSize; ++i){
		printf("arr[%3d] = %5d\n",i,arr[i]);
	}
}




