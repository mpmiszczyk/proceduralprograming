#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define ARR_SIZE 1000
#define MIN_WARTOSC 0.0
#define MAX_WARTOSC 1.0

void populateWithRandom (float arr[], int arrSize, float min, float max);
float  randomFrom(float min,float max);
int findIndexOfClosest(const float arr[],int arrSize,float wartosc);
int isCloser(float new,float old,float wartosc);
float module(float val);

int main (void){


	float arr [ARR_SIZE];
	float wartosc;

	populateWithRandom(arr,ARR_SIZE,MIN_WARTOSC,MAX_WARTOSC);

	do {
		printf ("prosze podac liczbe z przedzialu %f  %f\n",MIN_WARTOSC,MAX_WARTOSC);
		scanf(" %f",&wartosc);
	} while (!(MIN_WARTOSC <= wartosc && wartosc <= MAX_WARTOSC));

	
	int index = findIndexOfClosest(arr,ARR_SIZE,wartosc);


	printf ("najblizsza wylosowana wartosc wynosi %g i znajduje sie na %d miejscu tablicy\n", arr[index], index +1);

	

	return 0;
}



void populateWithRandom (float arr[], int arrSize, float min, float max){
	
	for (int i=0; i<arrSize; ++i){
		arr[i] = randomFrom(min, max);
	}	
}

float randomFrom(float min,float max){
	static time_t date;
	static char singleton; //inicjalizowany jako zero
	
	if (singleton==0){
		singleton = 1;
		srand(time(&date));
	}

	return (rand() / (float)RAND_MAX) * (max - min)  + min;
}


int findIndexOfClosest(const float arr[],int arrSize,float wartosc){
	int index = 0;
	
	for (int i=1; i<arrSize; ++i){
		if ( isCloser(arr[i],arr[index],wartosc) ){
			index = i;
		}
	}

	return index;
}

int isCloser(float new,float old,float wartosc){

	float newWar = wartosc - new;
	float oldWar = wartosc - old;
	
	if ( module(newWar) < module (oldWar) ){
		return 1;
	}
	return 0;
}

float module(float val){
	if (val >= 0){
		return val;
	} else {
		return -val;
	}
}

