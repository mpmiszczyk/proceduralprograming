#include <stdlib.h>
#include <stdio.h>

typedef struct T_Complex {
	int rl;
	int im;
} * Complex;

Complex new_Complex(int rl, int im);

int main (int argc, char ** argv){

	Complex comp = new_Complex (1,2);

	printf ("comp rl: %d   comp im:  %d \n",comp->rl,comp->im);
/* generate error
	printf ("comp rl: %d   comp im:  %d \n",comp.rl,comp.im);
*/

	return 0;
}


Complex new_Complex(int rl, int im){
	Complex comp = (Complex) malloc (sizeof(struct T_Complex));
	comp->rl = rl;
	comp->im = im;
	return comp;
}


