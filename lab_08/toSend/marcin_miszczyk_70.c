#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_ROW_SIZE 100
#define MAX_ROWS 100

struct Matrix{
	float **arr;
	unsigned int rows;
	unsigned int rowsSize;
};



struct Matrix getNewMatrix();
struct Matrix new_Matrix  (unsigned int rows, unsigned int rowsSize);
void free_Matrix  (struct Matrix *matrix );

void populateMatrix(struct Matrix * matrix);
void populateMatrixWithRandom (struct Matrix *matrix);
void insertValues (struct Matrix * matrix);

void printMatrix(const struct Matrix * matrix);


struct Matrix multiplyMatrix(struct Matrix *matrix1,struct Matrix *matrix2);


void populateArrWithRandom(float tab[],unsigned int tabSize, int bottom, int top);
float randomFloat(int bottom, int top);


void errorHandler (const char * message);

int main (void){
	
	struct Matrix matrix1,matrix2,matrix3;

	printf("inicjalizacja PIERWSZEJ tablicy\n");
	matrix1 = getNewMatrix(); 
	populateMatrix(&matrix1);

	printf("\n");
	printf("inicjalizacja DRUGIEJ tablicy\n");
	matrix2 = getNewMatrix(); 
	populateMatrix(&matrix2);



	printMatrix(&matrix1);
	puts("");
	printMatrix(&matrix2);

	matrix3 = multiplyMatrix(&matrix1,&matrix2);

	if ( !(matrix3.rows == 0 && matrix3.rowsSize == 0))	{
		printf("\nMacierz wynikowa\n");
		printMatrix(&matrix3); 
	} else {
		//TODO wymyślić logikę obsługi niepoprawnych danych
		//jak na razie jedyny pomysł to goto którego nie chcę urzywać
		//lub rozrzucenie kodu po kilku plikach co powinno pozwolić na budowanie bardziej elestycznej architektury 
		
	}

	free_Matrix(&matrix1);
	free_Matrix(&matrix2);
	free_Matrix(&matrix3);

	

	return 0;
	
}


struct Matrix getNewMatrix(){
	unsigned int rows,rowsSize;

	do {
		printf("Proszę podać ilość wierszy krótszą od %d \n",MAX_ROWS);
		scanf(" %u",&rows);
	}while (!( rows <= MAX_ROWS ));

	do {
		printf("Proszę podać długość wierszy krótszą od %d \n",MAX_ROW_SIZE);
		scanf(" %u",&rowsSize);
	}while (!( rowsSize <= MAX_ROW_SIZE ));


	return new_Matrix (rows, rowsSize);
	
}



struct Matrix new_Matrix (unsigned int rows, unsigned int rowsSize){

	struct Matrix arr;

	if (rows <= MAX_ROWS && rowsSize <= MAX_ROW_SIZE){

		arr.rows = rows;
		arr.rowsSize = rowsSize;


		if (( arr.arr = (float**)malloc(arr.rows * sizeof(float*)) ) == NULL ){	
			arr.rows = 0;
			errorHandler("błąd przy próbie alokacji pamieci dla wskaźnieków do tablic");		
		} 
	 


		for (int i=0; i< rows; ++i ){
			if ((arr.arr[i] = (float*) malloc(arr.rowsSize * sizeof (float))) == NULL ){
				arr.rows = i;
				free_Matrix(&arr);
				char buff[45]; 
				sprintf(buff,"błąd alokacji wiersza %10d wielkości %10d\n",i+1,arr.rowsSize);
				errorHandler(buff);
			}				
		}
	} else {
		arr.rows = 0;
		arr.rowsSize = 0;
		errorHandler("próba stworzenia tablicy poza limitami pamięci");
	}

	return arr;
}

void free_Matrix  (struct Matrix *matrix ){
	
	for (int i= matrix->rows -1 ; i>=0 ;--i){
		free( matrix->arr[i]);
	}
		free (matrix->arr);
}


void populateMatrix(struct Matrix * matrix){

	printf("czy wypełnić tablicę liczbami losowymi ? [Y/n]\n");
	char anw[20]; 
	scanf ("%s",anw);
	printf("%s\n",anw);
	if ( !(anw[0] == 'n' || anw[0] == 'N') ){
		populateMatrixWithRandom(matrix);	
	} else{
		insertValues(matrix);
	}		
}


void populateMatrixWithRandom (struct Matrix *matrix){
	float top,bottom;

	do {
		printf("prosze podać dolną i górną granice losowania liczb\n");
		scanf (" %f %f",&bottom,&top);
	}while(bottom > top);  //TODO kod ma tendencje do zapętlania się przy nieliczbowych danych

	for (int i=0; i < matrix->rows; ++i){
		populateArrWithRandom (matrix->arr[i],matrix->rowsSize, bottom , top);
	}

}


void populateArrWithRandom(float tab[],unsigned int tabSize, int bottom, int top){
	
	static time_t date;
	static int singleton = 1;
		
	if (singleton){
		singleton = 0;
		srand(time(&date));
	}

	for (int i =0; i< tabSize; ++i){
		tab[i]=randomFloat( bottom , top);
	}
}


float randomFloat(int bottom, int top){

	static time_t date;
	static int singleton = 1;
		
	if (singleton){
		singleton = 0;
		srand(time(&date));
	}

	return (rand() / (float)RAND_MAX) * (top - bottom)  + bottom;
}


void insertValues (struct Matrix * matrix){
	printf ("macierz o wymiarach %d wierszy na %d kolumny\n"
					,matrix->rows, matrix->rowsSize);
	printf ("prosze podawać kolejne wartości dla danego wiersza\n");
	
	for (int row = 0; row < matrix->rows; ++row){
		printf ("wiersz %d :  ",row);
		for (int i = 0; i < matrix->rowsSize; ++i){
			scanf (" %f", &(matrix->arr[row][i]));
		}
		printf("\n");
	}
}



void printMatrix(const struct Matrix * matrix){
	for (int i=0; i < matrix->rows; ++i){
		printf("wiersz %3d\n",i+1);
		for (int k=0; k < matrix->rowsSize; ++k){
			printf("%3.1f     ",matrix->arr[i][k]);
		}
		printf("\n");
	}
}


struct Matrix multiplyMatrix(struct Matrix *matrix1,struct Matrix *matrix2){
	struct Matrix multiplyMx;
	
	if (matrix1->rowsSize != matrix2->rows){
		printf("Błędne wymiary macierzy; mnożenie niedozwolone\n");
		multiplyMx = new_Matrix(0,0);
	} else {
		multiplyMx = new_Matrix(matrix1->rows, matrix2->rowsSize);

		for (int row = 0; row < multiplyMx.rows; ++row){
		for (int kol = 0; kol < multiplyMx.rowsSize; ++kol){
	
			float sum = 0;	
								
			for (int i =0; i < matrix1->rowsSize; ++i){
				sum += (matrix1->arr[row][i]) * (matrix2->arr[i][kol]);
			}
			
			multiplyMx.arr[row][kol] = sum;

		}
		} 
	}

	return multiplyMx;
}

void errorHandler (const char * message){
	fprintf(stderr,message);
	exit(EXIT_FAILURE);
}
