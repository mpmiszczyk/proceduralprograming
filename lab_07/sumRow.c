#include <stdio.h>
#define ROW_SIZE 3
#define COLUMN_SIZE 4

int sumujTablice(const int tab[],int size);
int diagonalSum(const int tab[][ROW_SIZE],int rowSize,int columnSize);

int main (int argc, char **argv){

	int tab[COLUMN_SIZE][ROW_SIZE]={{1,2},{3,4},{5},{6,7}};
	int const (*temp)[ROW_SIZE] = tab; // TODO nieprawidlowa inicjalizacja
	int row;
	int suma,sumAll,slad;

	printf("prosze podac wiersz do zsumowania ");
	do {
		printf("wiersz musi buc mniejszy od %d\n",COLUMN_SIZE);
		scanf(" %d",&row);
		row--;
	} while(!( 0 <= row && row < COLUMN_SIZE));

	suma = sumujTablice(tab[row],ROW_SIZE);

	printf ("suam elementow wiersza %d to %d\n", row+1, suma);


	sumAll=0;
	for (int i=0; i<COLUMN_SIZE;++i){
		sumAll += sumujTablice(tab[i],ROW_SIZE);
	}
	printf ("\nsuma wszystkich elementow to %d\n", sumAll);
	

	sumAll=0;
	sumAll += sumujTablice((int*)tab,ROW_SIZE*COLUMN_SIZE);
	printf ("\nsuma wszystkich elementow to %d\n", sumAll);

	slad = diagonalSum(tab,ROW_SIZE,COLUMN_SIZE);
	printf ("\nslad tablicy to %d\n", slad);
	slad = diagonalSum(tab,ROW_SIZE,COLUMN_SIZE);

	

	return 0;
}



int sumujTablice(const int tab[],int size){
	int sum=0;

	for (int i =0; i < size; ++i){
		sum += tab[i];		
	}
	
	return sum;	
}

int diagonalSum(const int tab[][ROW_SIZE],int rowSize,int columnSize){
	int sum =0;

	for (int i=0; i<rowSize && i< columnSize; ++i){
		sum += tab[i][i];
	}
	
	return sum;
}


