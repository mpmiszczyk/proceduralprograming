#include <stdio.h>
#define ROW 3
#define COLUMN 4

void function(int * tab, int size );

int main (int argc, char **argv){

	int tab[COLUMN][ROW]={{1,2},{3,4},{5},{6,7}};
	
	printf("tab       = %p\n",tab);
	printf("*tab      = %p\n",*tab);	
	printf("tab[0]    = %p\n",tab[0]);	
	printf("&tab[0]   = %p\n",&tab[0]);
	printf("*tab[0]   = %p\n",*tab[0]);
	printf("&tab[0][0] = %p\n",&tab[0][0]);	
	printf("tab[0][0] = %p\n",tab[0][0]);

	printf("\n");

	printf("tab[0]    = %p\n",tab[0]);	
	printf("&tab[0]   = %p\n",&tab[0]);
	printf("*tab[0]   = %p\n",*tab[0]);

	printf("tab[1]    = %p\n",tab[1]);	
	printf("&tab[1]   = %p\n",&tab[1]);
	printf("*tab[1]   = %p\n",*tab[1]);
	printf("tab[0] +1   = %p\n",tab[0] +1 );	
	printf("*tab + 1    = %p\n",*tab + 1);	
	printf("*(tab + 1)  = %p\n",*(tab + 1));


	printf("\n");

	for (int i=0; i<COLUMN*ROW; ++i){
		printf("tab[0][%d] = %d\n",i,tab[0][i]);
	}

	printf("\n");

	for (int i=0; i<COLUMN; ++i){
	for (int k=0; k<ROW; ++k){
		printf("tab[%d][%d]         = %d\n",i,k,tab[i][k]);
		printf("*(*(tab + %d) + %d) = %d\n",i,k,*(*(tab + i)+k));
	}}printf("\n");

	function( tab[0] , COLUMN*ROW);
	function( (int*)tab , COLUMN*ROW);



	return 0;
}



void function(int * tab , int size ){

	printf("\n");
	printf("wewnatrz funkcji\n");

	for (int i=0; i<COLUMN*ROW; ++i){
		printf("func  tab[%d] = %d\n",i,tab[i]);
	}
	
}
