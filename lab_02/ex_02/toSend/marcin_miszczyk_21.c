#include <stdio.h>

int main()
{
	char znak;
	int mCount = 0,
		iCount = 0,
		sCount = 0,
		zCount = 0,
		cCount = 0,
		yCount = 0,
		kCount = 0;
		

	do{
		scanf("%c",&znak);
		switch (znak){
			case 'M':
			case 'm':
				mCount++;
				break;
			case 'I':
			case 'i':
				iCount++;
				break;
			case 'S':
			case 's':
				sCount++;
				break;
			case 'Z':
			case 'z':
				zCount++;
				break;
			case 'C':
			case 'c':
				cCount++;
				break;
			case 'Y':
			case 'y':
				yCount++;
				break;
			case 'K':
			case 'k':
				kCount++;
				break;
			default:
				printf("%c",znak);
		}

	} while (znak != '\n');

	printf("\npodsumowanie wystapien:\n"
	"m: %5d i: %5d s: %5d z: %5d c: %5d y: %5d k: %5d ",
	 mCount, iCount, sCount, zCount, cCount, yCount, kCount);

	return 0;

}
