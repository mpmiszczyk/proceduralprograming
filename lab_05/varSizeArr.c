#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

#define ROZMIAR 100000
#define ROW_SIZE 4



void printArr(const float tab[],unsigned  int tabSize);
void diffrentPrintArr(const float tab[],unsigned int tabSize, int rowSize);


void populateWithRandom(float tab[],unsigned int tabSize, int bottom, int top);
int randomInt(int bottom, int top);
float randomFloat(int bottom, int top);

void sortW (float tab[], unsigned int tabSize);
void sortWpionter (float *tab, unsigned int tabSize);

void swap (float *a, float *b);
float maxValue (const float tab[], unsigned int tabSize);
float minValue (const float tab[], unsigned int tabSize);
double avgValue (const float tab[], unsigned int tabSize);
double avgValueNoOwerFlow (const float tab[],unsigned  int tabSize);

void normalizeArr(float tab[],unsigned int tabSize);

int main(void){

	float *tab;
	int  bottom, top;
	unsigned int tabSize;
	

	printf("prosze o podanie wielkosci tablicy\n");
	scanf(" %d", &tabSize);

	printf("tabSize %d\n",tabSize);
	
	tab = (float*)malloc(tabSize * sizeof(tab[0]));
	
	if (tab == NULL){
		exit(EXIT_FAILURE);
	}


	printf("\n");
	printf("prosze o podanie dolnego i gornego zakresu liczb losowych\n");
	scanf(" %d %d", &bottom, &top);
	
	populateWithRandom(tab,tabSize,bottom,top);	

	printf("nieposortowana tablica:\n");
	diffrentPrintArr(tab,tabSize,ROW_SIZE);

	printf("\n");
	printf("sortowanie ...\n\n");
	sortW(tab,tabSize);
	
	printf("posortowana tablica:\n");
	diffrentPrintArr(tab,tabSize,ROW_SIZE);

	
	printf("\n");
	printf("najwieksza wartosc :  %20d\n",maxValue(tab,tabSize));
	printf("najmniejsza wartosc : %20d\n",minValue(tab,tabSize));
	printf("srednia wartosc :     %20g\n",avgValue(tab,tabSize));
	printf("srednia wartosc2 :    %20g\n",
				avgValueNoOwerFlow(tab,tabSize));
	printf("roznica w srednich :  %20g\n",
				avgValue(tab,tabSize) - avgValueNoOwerFlow(tab,tabSize));


	printf("\n");
	printf("normalizacja ...\n\n");
	normalizeArr(tab,tabSize);

	printf("znormalizowana tablica:\n");
	diffrentPrintArr(tab,tabSize,ROW_SIZE);

	float sum = 0;
	for (int i = 0; i< tabSize; i++){
		sum += tab[i];
	}
	printf("suma wszystkich elementow: %g\n",sum);

	free(tab);
	return 0;
}

void populateWithRandom(float tab[],unsigned int tabSize, int bottom, int top){
	
	static time_t date;
	static int singleton = 1;
		
	if (singleton){
		singleton = 0;
		srand(time(&date));
	}

	for (int i =0; i< tabSize; ++i){
		tab[i]=randomFloat( bottom , top);
	}
}

int randomInt(int bottom, int top){

	static time_t date;
	static int singleton = 1;
		
	if (singleton){
		singleton = 0;
		srand(time(&date));
	}

	return rand() % (top - bottom) + bottom;
	
}

float randomFloat(int bottom, int top){

	static time_t date;
	static int singleton = 1;
		
	if (singleton){
		singleton = 0;
		srand(time(&date));
	}

	return (rand() / (float)RAND_MAX) * (top - bottom)  + bottom;
}

void printArr(const float tab[],unsigned  int tabSize){
	for (int i =0; i< tabSize; ++i){
		printf("tab[%3d] = %15.5f\n",i,tab[i]);
	}
}

void diffrentPrintArr(const float tab[],unsigned int tabSize, int rowSize){
	for (int i =0; i< tabSize; ++i){
		printf("%15.5f",tab[i]);
		if ((i + 1) % rowSize == 0){
			printf("\n");
		}
	}

	printf("\n");
}

void sortW (float tab[],unsigned  int tabSize){

	for (int i = 1; i<tabSize; ++i ){
		for (int j = i; j > 0; --j){

			if ( tab[j-1] > tab [j] ){
				swap (&tab[j], &tab[j-1]);
			}else{
				break;
			}
		}
	}
}



void sortWpionter (float *tab,unsigned int tabSize){

	for (int i = 1; i<tabSize; ++i ){
		for (int j = i; j > 0; --j){

			if ( *(tab + j - 1) > *(tab + j) ){
				swap ( tab + j, tab + j -1);
			}else{
				break;
			}
		}
	}
}

void swap (float *a, float *b){
	float temp = *a;
	*a = *b;
	*b = temp;
}



float maxValue (const float tab[],unsigned  int tabSize){
	float max = FLT_MAX;
	for (int i = 0; i<tabSize; ++i ){
		max = tab[i] > max ? tab[i] : max;
	}
	return max;
}

float minValue (const float tab[],unsigned  int tabSize){
	float min = FLT_MIN;
	for (int i = 0; i<tabSize; ++i ){
		min = tab[i] < min ? tab[i] : min;
	}
	return min;
}



double avgValue (const float tab[],unsigned  int tabSize){
	double sum = 0;
	for (int i = 0; i<tabSize; ++i ){
		sum += (double)tab[i];
	}

	return	sum/tabSize;
}
double avgValueNoOwerFlow (const float tab[],unsigned  int tabSize){
	double avg = 0;
	for (int i = 0; i<tabSize; ++i ){
		avg += ((double)tab[i] / (double)tabSize);
	}

	return	avg;
}


void normalizeArr(float tab[],unsigned int tabSize){
	double sum = 0;
	
	for (int i = 0; i<tabSize ; i++){
		sum += tab[i];
	}
	for (int i = 0; i<tabSize ; i++){
		tab[i] /= sum;
	}
}



