#include <stdio.h>
int main(void)
{
	int n, p, q;
	n = 5; p = 2;
	q = n++ > p || p++ !=3; /* wypisz n, p, q */
	printf(" n= %d   p= %d   q= %d \n", n, p, q);
	//         6         2       1

	n = 5; p = 2;
	q = n++ <p || p++ !=3;     /* wypisz n, p, q */
	printf(" n= %d   p= %d   q= %d \n", n, p, q);
	//          6        3      1 

	n = 5; p = 2;
	q = ++n == 3 && ++p == 3;        /* wypisz n, p, q */
	printf(" n= %d   p= %d   q= %d \n", n, p, q);
	//          6        2       0

	n = 5; p = 2;
	q = ++n == 6 && ++p == 3; /* wypisz n, p, q */
	printf(" n= %d   p= %d   q= %d \n", n, p, q);
	//          6        3       1


	return 0;
}
