#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define true 1
#define false 0


#define MAX_LINE 100

void handleError (const char *mess);

int countBigCapps(const char * str);
int countIn (const char * str, int (*condition)(int c) );

int Ile_slow (const char *str );


int wordStart(const char * str);
int isWordStart (char ch);

int wordEnd(const char * str);

void Hcucnal (char *str );

int main (void){
	
	char line[MAX_LINE];

	puts("Proszę podać linię do analizy");
	if (fgets(line,MAX_LINE - 1, stdin) == NULL){
		handleError("bląd pobrania lini");
	}

	printf("liczba duzych znaków %d\n",countBigCapps(line));

	printf("liczba maluch znaków %d\n",countIn(line,islower));
	printf("liczba znaków interpukcyjnych znaków %d\n",countIn(line,ispunct));
	printf("liczba cyfr %d\n",countIn(line,isdigit));
	printf("liczba białych znaków %d\n",countIn(line,isspace));

	puts("Twoja linia po odwróceniu");
	Hcucnal( line);
	puts("34");
	printf("%s\n",line);

	return 0;
}



int countBigCapps(const char * str){
	return countIn(str,isupper);
}

int countIn (const char * str, int (*condition)(int c) ){
	int count =0;
	for (int i =0; i < strlen(str) ; ++i){ 
		if (condition(str[i]))
			++count;
	}
	return count;
}



int Ile_slow (const char *str ){
	int count = 0;

	int strLen = strlen (str);

	int i = 0;	
	while ( i < strLen){
		if ( i += wordStart(str + i)){
			++count;
		}
		i += wordEnd(str + i);
	}
	

	return count;
}

int wordStart(const char * str){
	int strLen = strlen (str);
	int i = 0;
	
	char lastChar = ' ';

	while ( i < strLen){
		if (isspace(lastChar) && isWordStart(str[i])){
			break;
		}else {
			lastChar = str[i];
			+i;
		}
	}
	
	return i;
}

int isWordStart (char ch){
	return (ch == '_' || isalpha(ch));
}

int wordEnd(const char * str){
	int strLen = strlen (str);
	int i = 0;

	while ( i < strLen){
		if (!isspace(str[i])){
			break;
		++i;
	}

	return i;
}

void Hcucnal( char *str ){
	char *temp;
	
	int strLen = strlen (str);
	
	
	if ((temp = (char*) malloc (sizeof(str[0]) * strLen)) == NULL){
		handleError("błąd przypisania pamięci przy rezerwawaniu tymczasowego stringu");
	}
	
	for (int i =0; i<strLen ; ++i){
		temp[i] = str[strLen - 1 - i];
	}
	for (int i =0; i<strLen ; ++i){
		str[i] = temp[i];
	}

	free(temp);
	
}


void handleError (const char *mess){
	puts(mess);
	exit(-1);
}
