#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>


#define MAX_LINE 100

void handleError (const char *mess);

int countBigCapps(const char * str);
int countIn (const char * str, int (*condition)(int c) );

void Hcucnal( char *str );

int main (void){
	
	char line[MAX_LINE];

	puts("Proszę podać linię do analizy");
	if (fgets(line,MAX_LINE - 1, stdin) == NULL){
		handleError("bląd pobrania lini");
	}

	printf("liczba duzych znaków %d\n",countBigCapps(line));

	printf("liczba maluch znaków %d\n",countIn(line,islower));
	printf("liczba znaków interpukcyjnych znaków %d\n",countIn(line,ispunct));
	printf("liczba cyfr %d\n",countIn(line,isalnum));
	printf("liczba białych znaków %d\n",countIn(line,isspace));

	puts("Twoaj linia po odwróceniu");
	Hcucnal( line);
	printf("%s",line);

	return 0;
}



int countBigCapps(const char * str){
	return countIn(str,isupper);
}

int countIn (const char * str, int (*condition)(int c) ){
	int count =0;
	for (int i =0; i < strlen(str) ; ++i){ 
		if (condition(str[i]))
			++count;
	}
	return count;
}

void Hcucnal( char *str ){
	char *temp;
	
	int strLen = strlen (str);
	
	
	if ((temp = (char*) malloc (sizeof(str[0]) * strLen) == NULL){
		handleError("błąd przypisania pamięci przy rezerwawaniu tymczasowego stringu");
	}
	
	for (int i =0; i<strLen ; ++i){
		temp[i] = str[strLen - i];
	}
	for (int i =0; i<strLen ; ++i){
		str[i] = temp[i];
	}
	
}


void handleError (const char *mess){
	puts(mess);
	exit(-1);
}
