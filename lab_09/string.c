#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(){
  char s[] = "Ala ma kota";
  char *p = malloc(30);
  
  strcpy(p,"napis: ");
  printf("%s\n%s\n", s, p);

  strcat(p,s);
  printf("%s\n", p);

  printf("%d\n", strcmp(p,s));
  printf("%d\n", strcmp(s,p));
  strcpy(p,s);
  printf("%d\n", strcmp(p,s));

  printf("%ld\n", strlen(p));

  return 0;
}

