#include <stdio.h>

int main (void){

	int tabA[10];
	double tabB[10];

	//pointerA = tabA;
	//pointerB = tabB;

	
	printf ("adresy pierwszej komurki : %14p \n", &tabA[0]);
	printf ("adresy drugiej   komurki : %14p \n", &tabA[1]);
	printf ("adresy trzeciej  komurki : %14p \n", &tabA[2]);
	printf ("adresy czwartej  komurki : %14p \n", &tabA[3]);
	
	printf("\n");
	printf("sizeof int                       : %5x\n",sizeof (int));
	printf("roznica pomiedzy adresami pamieci: %5x\n", &tabA[1] - &tabA[0]);	


	

	printf ("adresy pierwszej komurki : %14p \n", &tabB[0]);
	printf ("adresy drugiej   komurki : %14p \n", &tabB[1]);
	printf ("adresy trzeciej  komurki : %14p \n", &tabB[2]);
	printf ("adresy czwartej  komurki : %14p \n", &tabB[3]);
	
	printf("\n");
	printf("sizeof double                    : %5x\n",sizeof (double));
	printf("roznica pomiedzy adresami pamieci: %5x\n", &tabB[1] - &tabB[0]);
	



	return 0;
}
