#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define ROZMIAR 100000



void printArr(const int tab[], int tabSize);
void populateWithRandom(int tab[],int tabSize);


void coppyArr(const int tab[], int tabCopy[], int tabSize);
int compareArr(const int tab[], const int tabCopy[], int tabSize);

void sortW (int tab[], int tabSize);
void sortWpionter (int *tab, int tabSize);

void swap (int *a, int *b);
int maxValue (const int tab[], int tabSize);
int minValue (const int tab[], int tabSize);
double avgValue (const int tab[], int tabSize);
double avgValueNoOwerFlow (const int tab[], int tabSize);

int main(void){

	time_t date;

	int tab[ROZMIAR];
	int tabCopy[ROZMIAR];

	int tabSize = sizeof(tab) / sizeof (tab[0]);
	int tabCopySize = sizeof(tabCopy) / sizeof (tabCopy[0]);
	printf("tabSize %d\n",tabSize);
	
	populateWithRandom(tab,tabSize);	

	printf("nieposortowana tablica:\n");
	printArr(tab,tabSize);
	coppyArr(tab,tabCopy,tabSize);

	printf("\n");
	printf("sortowanie ...\n\n");
	sortW(tab,tabSize);
	
	printf("posortowana tablica:\n");
	printArr(tab,tabSize);

	printf("\n");
	printf("sortowanie 2 ...\n\n");
	sortWpionter(tabCopy,tabCopySize);

	compareArr(tab,tabCopy,tabSize);
	
	printf("\n");
	printf("najwieksza wartosc :  %20d\n",maxValue(tab,tabSize));
	printf("najmniejsza wartosc : %20d\n",minValue(tab,tabSize));
	printf("srednia wartosc :     %20g\n",avgValue(tab,tabSize));
	printf("srednia wartosc2 :    %20g\n",
				avgValueNoOwerFlow(tab,tabSize));
	printf("roznica w srednich :  %20g\n",
				avgValue(tab,tabSize) - avgValueNoOwerFlow(tab,tabSize));

	return 0;
}

void populateWithRandom(int tab[],int tabSize){
	
	static time_t date;
	static int singleton = 1;
		
	if (singleton){
		singleton = 0;
		srand(time(&date));
	}

	for (int i =0; i< tabSize; ++i){
		tab[i]=rand();
	}
	
}

void printArr(const int tab[], int tabSize){
	for (int i =0; i< tabSize; ++i){
		printf("tab[%3d] = %12d\n",i,tab[i]);
	}
}

void sortW (int tab[], int tabSize){

	for (int i = 1; i<tabSize; ++i ){
		for (int j = i; j > 0; --j){

			if ( tab[j-1] > tab [j] ){
				swap (&tab[j], &tab[j-1]);
			}else{
				break;
			}
		}
	}
}


void coppyArr(const int tab[], int tabCopy[], int tabSize){
	for(int i =0; i < tabSize; i++){
		tabCopy[i] = tab[i];
	}
}

int compareArr(const int tab[], const int tabCopy[], int tabSize){
	for(int i =0; i < tabSize; i++){
		if ( tabCopy[i] != tab[i] ){
			printf("!!rozne wartosci!!\n"
		" tab[%d]    = %12d\n"
		" tabCopy[%d]= %12d\n", i, tab[i], i, tabCopy[i]);
		return 0;
		}
	}
	puts("tabliec posortowane identycznie");
	return 1;
}

void sortWpionter (int *tab, int tabSize){

	for (int i = 1; i<tabSize; ++i ){
		for (int j = i; j > 0; --j){

			if ( *(tab + j - 1) > *(tab + j) ){
				swap ( tab + j, tab + j -1);
			}else{
				break;
			}
		}
	}
}

void swap (int *a, int *b){
	int temp = *a;
	*a = *b;
	*b = temp;
}



int maxValue (const int tab[], int tabSize){
	int max = INT_MIN;
	for (int i = 0; i<tabSize; ++i ){
		max = tab[i] > max ? tab[i] : max;
	}
	return max;
}

int minValue (const int tab[], int tabSize){
	int min = INT_MAX;
	for (int i = 0; i<tabSize; ++i ){
		min = tab[i] < min ? tab[i] : min;
	}
	return min;
}



double avgValue (const int tab[], int tabSize){
	double sum = 0;
	for (int i = 0; i<tabSize; ++i ){
		sum += (double)tab[i];
	}

	return	sum/tabSize;
}
double avgValueNoOwerFlow (const int tab[], int tabSize){
	double avg = 0;
	for (int i = 0; i<tabSize; ++i ){
		avg += ((double)tab[i] / (double)tabSize);
	}

	return	avg;
}




