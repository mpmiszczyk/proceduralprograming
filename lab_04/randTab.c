#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define ROZMIAR 1000


void sortW (int tab[], int tabSize);
void swap (int *a, int *b);
int maxValue (const int tab[], int tabSize);
int minValue (const int tab[], int tabSize);
double avgValue (const int tab[], int tabSize);
double avgValueNoOwerFlow (const int tab[], int tabSize);

int main(void){

	time_t date;

	int tab[ROZMIAR];

	int tabSize = sizeof(tab) / sizeof (tab[0]);
	printf("tabSize %d\n",tabSize);

	srand(time(&date));
	
	for (int i =0; i< tabSize; ++i){
		tab[i]=rand();
		printf("tab[%3d] = %20d\n",i,tab[i]);
	}

	printf("\n");
	sortW(tab,tabSize);

	for (int i =0; i< tabSize; ++i){
		printf("tab[%3d] = %20d\n",i,tab[i]);
	}

	printf("\n");
	printf("najwieksza wartosc :  %20d\n",maxValue(tab,tabSize));
	printf("najmniejsza wartosc : %20d\n",minValue(tab,tabSize));
	printf("srednia wartosc :     %g\n",avgValue(tab,tabSize));
	printf("srednia wartosc2 :    %g\n",avgValueNoOwerFlow(tab,tabSize));
	printf("roznica w srednich :  %g\n",avgValue(tab,tabSize) - avgValueNoOwerFlow(tab,tabSize));

	return 0;
}

void sortW (int tab[], int tabSize){

	for (int i = 1; i<tabSize; ++i ){
		for (int j = i; j > 0; --j){

			if ( tab[j-1] > tab [j] ){
				swap (&tab[j], &tab[j-1]);
			}else{
				break;
			}
		}
	}
}

void swap (int *a, int *b){
	int temp = *a;
	*a = *b;
	*b = temp;
}



int maxValue (const int tab[], int tabSize){
	int max = INT_MIN;
	for (int i = 0; i<tabSize; ++i ){
		max = tab[i] > max ? tab[i] : max;
	}
	return max;
}
 
int minValue (const int tab[], int tabSize){
	int min = INT_MAX;
	for (int i = 0; i<tabSize; ++i ){
		min = tab[i] < min ? tab[i] : min;
	}
	return min;
}



double avgValue (const int tab[], int tabSize){
	double sum = 0;
	for (int i = 0; i<tabSize; ++i ){
		sum += (double)tab[i];
	}

	return	sum/tabSize;
}

double avgValueNoOwerFlow (const int tab[], int tabSize){
	double avg = 0;
	for (int i = 0; i<tabSize; ++i ){
		avg += ((double)tab[i] / (double)tabSize);
	}

	return	avg;
}

