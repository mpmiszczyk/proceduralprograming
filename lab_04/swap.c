#include <stdio.h>

void swap (int *a, int *b);
void swap2 (int *a, int *b);

int main (void){

	int a,b;

	a = 10;
	b = 99;

	printf ("wartosci zmiennych :\n a: %10d b: %10d\n\n", a,b);

	swap (&a,&b);

	printf ("wartosci zmiennych :\n a: %10d b: %10d\n\n", a,b);

	swap2 (&a,&b);

	printf ("wartosci zmiennych :\n a: %10d b: %10d\n", a,b);
	printf ("durgi swap nie dziala bo adresy zmiennych a i b sa STALYMI\n i nie mozna im przypisac nowych wartosci\n");

	return 0;
}


void swap (int *a, int *b){
	int temp;
	temp = *a;
	*a = *b;
	*b = temp;
	
}


//nie zdziala tak jak mozna by sie spodziewac
void swap2 (int *a, int *b){
	int *temp;
	temp = a;
	a = b;
	b = temp;
}
