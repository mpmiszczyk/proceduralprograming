#include <stdio.h>

int main (void){

	int tab[10];


	printf("adres tablicy: %p; \nadres pierwszego elementu: %p \n"
		,tab,&tab[0]);


	printf("\nadresy pol w tablicy\n");

	for(int i=0; i<10; ++i){
		printf("%d %14d %14d\n",
			i,
			&tab[i],
			tab +i);
	}

	printf("\nwartosci w tablicy\n");

	for(int i=0; i<10; ++i){
		printf("%d %14d %14d\n",
			i,
			tab[i],
			*(tab +i));
	}

	return 0;
}
