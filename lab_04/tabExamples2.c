#include <stdio.h>

int main (void){

	int tabA[10];
	int tabB[10];
	
	int *pointerA;
	int *pointerB;

	//pointerA = tabA;
	//pointerB = tabB;

	
	printf ("adresy tablic              : a: %10p b: %10p\n", tabA,tabB);
	printf ("adresy adresow tablic      : a: %10p b: %10p\n", &tabA,&tabB);
	printf ("adresy pierwszego elementu : a: %10p b: %10p\n", &tabA[0],&tabB[0]);
	printf ("adresy pod wskaznikami     : a: %10p b: %10p\n", pointerA,pointerB);

	printf ("adresy wskaznikow          : a: %10p b: %10p\n", &pointerA,&pointerB);


	return 0;
}
