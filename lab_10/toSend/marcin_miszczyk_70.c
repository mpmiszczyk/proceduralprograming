#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define SIZE_LEN 20

struct Point {
	float x;
	float y;
};


struct Point getRandomPoint(void);
float randomFrom(float min,float max);

int inCircle(struct Point point);

int main (void){

	int dokl;
	int count;

	printf("prosze podać liczbę iteracji\n");
	scanf(" %d",&dokl);

	for (int i=0; i<dokl; ++i){
		struct Point point = getRandomPoint();
		if (inCircle(point)){
			++count;
		}
	}

	float pi = 4.0 * (float)count /(float)dokl;


	printf("PI: %f\n",pi);


	
	


	return 0;
}



struct Point getRandomPoint(void){
	struct Point point;
	point.x = randomFrom(0,SIZE_LEN);
	point.y = randomFrom(0,SIZE_LEN);

	return point;
}

float randomFrom(float min,float max){
	static time_t date;
	static char singleton; //inicjalizowany jako zero
	
	if (singleton==0){
		singleton = 1;
		srand(time(&date));
	}

	return (rand() / (float)RAND_MAX) * (max - min)  + min;
}


int inCircle(struct Point point){
	return ( SIZE_LEN > sqrt((point.x * point.x) + (point.y * point.y)));
}
