#include <stdio.h>
#include <stdlib.h>
#include <time.h>


typedef struct t_FloatArray {
	float *arr;
	int length;
} FloatArray;

FloatArray new_FloatArray();
void free_FloatArray(FloatArray * p_floatArray);

void printArray (const FloatArray * arr);

void addRandomNumbers (FloatArray *p_arr,int count); 

void push (FloatArray *p_arr,float number);
float randomFrom(float min,float max);

void quickSort (FloatArray *p_arr);
int compareFloats (const void *a, const void *b);


void invertQuickSort (FloatArray *p_arr);
int invertCompareFloats (const void *a, const void *b);



int main (int agrc, char **agrv){

	FloatArray array = new_FloatArray();

	addRandomNumbers(&array,1000);
	printArray(&array);
	
	puts("\nSortowanie");
	invertQuickSort(&array);
	printArray(&array);

	free_FloatArray(&array);



	return 0;
}



FloatArray new_FloatArray(){
	FloatArray array;
	array.arr = (float*) malloc(sizeof(float));
	array.length = 0;
	return array;
}

void free_FloatArray(FloatArray * p_floatArray){
	p_floatArray->length = 0;
	free (p_floatArray->arr);
}


void printArray (const FloatArray * p_arr){
	for (int i=0; i < p_arr->length; ++i){
		printf("arr[%2d] = %6.3f\n",i,p_arr->arr[i]);
	}
}


void addRandomNumbers (FloatArray *p_arr, int count){
	for (int i=0; i < count; ++i){
		push ( p_arr, randomFrom(-2,5));
	}
}



void push (FloatArray *p_arr,float number){
	p_arr->length++;
	float *temp = realloc(p_arr->arr, p_arr->length * sizeof(p_arr->arr[0]));

	if (temp == NULL){
		p_arr->length--;
		fprintf(stderr, "błąd zalokowania pamięci dla tablicy %d floatów w strukturze FloatArray\n",p_arr->length);
		p_arr->length--;
	} else {
		p_arr->arr = temp;
		p_arr->arr[ p_arr->length -1] = number;
	}
}

float randomFrom(float min,float max){
	static time_t date;
	static char singleton; //inicjalizowany jako zero
	
	if (singleton==0){
		singleton = 1;
		srand(time(&date));
	}

	return (rand() / (float)RAND_MAX) * (max - min)  + min;
}




void quickSort (FloatArray *p_arr){
	qsort(p_arr->arr, p_arr->length, sizeof(p_arr->arr[0]), compareFloats);
}

int compareFloats (const void *a, const void *b){
	if  (*(float*)a > *(float*)b ){
		return 1;
	} else if (*(float*)a == *(float*)b) { 
		return 0;
	} else {
		return -1;
	}
}



void invertQuickSort (FloatArray *p_arr){
	qsort(p_arr->arr, p_arr->length, sizeof(p_arr->arr[0]), invertCompareFloats);
}

int invertCompareFloats (const void *a, const void *b){
	if  (*(float*)a > *(float*)b ){
		return -1;
	} else if (*(float*)a == *(float*)b) { 
		return 0;
	} else {
		return 1;
	}
}
