/*
 * QuickSort 
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct t_FloatArray {
	float *arr;
	int length;
} FloatArray;

FloatArray new_FloatArray();
void free_FloatArray(FloatArray * p_arr);

void printArray (const FloatArray * arr);

void addRandomNumbers (FloatArray *p_arr,int len); 
void push (FloatArray *p_arr,float number);
float randomFrom(float min,float max);

void quickSort (FloatArray * p_arr);

void quickSortReq (float *arr, int bottom, int top);
int partition (float *arr, int bottom, int top);
void swap(float *arr,int a,int b);

int main (void){

	FloatArray arr = new_FloatArray();

	addRandomNumbers(&arr,10);
	
	printf("tablica przed sortowaniem\n");
	printArray(&arr);

	printf("\n");
	printf("tablica po sortowaniem\n");

	quickSort(&arr); 
	printArray(&arr); 
	
	free_FloatArray (arr);
	
	return 0;
}



FloatArray new_FloatArray(){
	FloatArray array;
	array.arr = (float*) malloc(sizeof(float));
	array.length = 0;
	return array;
}


void free_FloatArray(FloatArray * p_arr){

	p_arr->length = 0;
	free (p_arr->arr);
}


void printArray (const FloatArray * p_arr){
	for (int i=0; i < p_arr->length; ++i){
		printf("arr[%2d] = %5.2f\n",i,p_arr->arr[i]);
	}
}


void addRandomNumbers (FloatArray *p_arr, int len){
	for (int i=0; i < len; ++i){
		push ( p_arr, randomFrom(0,100));
	}
}



void push (FloatArray *p_arr,float number){
	p_arr->length++;
	float *temp = realloc(p_arr->arr, p_arr->length * sizeof(p_arr->arr[0]));

	if (temp == NULL){
		p_arr->length--;
		fprintf(stderr, "błąd zalokowania pamięci dla tablicy %d floatów w strukturze FloatArray\n",p_arr->length);
		p_arr->length--;
	} else {
		p_arr->arr = temp;
		p_arr->arr[ p_arr->length -1] = number;
	}
}

float randomFrom(float min,float max){
	static time_t date;
	static char singleton; //inicjalizowany jako zero
	
	if (singleton==0){
		singleton = 1;
		srand(time(&date));
	}

	return (rand() / (float)RAND_MAX) * (max - min)  + min;
}



void quickSort (FloatArray * p_arr){
	quickSortReq(p_arr->arr, 0 , p_arr->length-1);
}

void quickSortReq (float *arr, int bottom, int top){

	if (bottom  < top){
		int mid = partition(arr, bottom, top);

	
		quickSortReq (arr, bottom, mid);
		quickSortReq (arr, mid + 1, top);
	}
}

int partition (float *arr, int bottom, int top){
	float key = arr[bottom];

	int smaller = bottom - 1;
	int greater = top + 1;

	while (1){

		do {
			++smaller;
		} while (arr[smaller] < key);

		do {
			--greater;
		} while (arr[greater] > key);

		if (smaller < greater){
			swap(arr,smaller,greater);
		} else {
			return greater;
		}
	}
}


void swap(float *arr,int a,int b){
	float temp = arr[a];
	arr[a] = arr[b];
	arr[b] = temp;
}

