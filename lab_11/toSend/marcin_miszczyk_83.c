#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARR_SIZE 10


void populateWithRandom(float * arr, int len);
float randomFrom(float min,float max);
void printArr(float * arr, int len);

long getFileSize(const char * fileName);


int main(void){

	float arr[ARR_SIZE];
	FILE *p_plik;
	
	
	populateWithRandom(arr,ARR_SIZE);	
	printArr(arr,ARR_SIZE);

	puts("\nzapisywanie do pliku data.txt");
	
	if((p_plik = fopen("data.txt","w"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu data.txt");
		exit(-1);
	}
	
	for(int i=0; i < ARR_SIZE ; ++i){
		fprintf(p_plik,"%f\n",arr[i]);
	}
	
	fclose(p_plik);


	
	puts("\nzapisywanie do pliku data.dat");

	if((p_plik = fopen("data.dat","wb"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu data.dat");
		exit(-1);
	}
	fwrite(arr, sizeof(arr[0]), ARR_SIZE, p_plik);	
	
	fclose(p_plik);



	puts("\nwypełnienie tablicy nowymi wartościami");
	populateWithRandom(arr,ARR_SIZE);	
	printArr(arr,ARR_SIZE);


	puts("\nodczytanie wartości z pliku data.txt");
	if((p_plik = fopen("data.txt","r"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu");
		exit(-1);
	}
	
	for(int i=0; i < ARR_SIZE; ++i){
		fscanf(p_plik,"%f",&(arr[i]));
	}
	printArr(arr,ARR_SIZE);


	puts("\nwypełnienie tablicy nowymi wartościami");
	populateWithRandom(arr,ARR_SIZE);	
	printArr(arr,ARR_SIZE);


	puts("\nodczytanie wartości z poliku data.dat");
	if((p_plik = fopen("data.dat","rb"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu");
		exit(-1);
	}
	
	fread(arr,sizeof(arr[0]),ARR_SIZE,p_plik);
	printArr(arr,ARR_SIZE);



	printf("\nDługość pliku data.txt : %ld\n",getFileSize("data.txt"));
	printf("Długość pliku data.dat : %ld\n",getFileSize("data.dat"));






	puts ("\nRóżnice pomiędzy wartościmi zapisanymi w plikach");

	FILE *p_dat,*p_txt;
	float aDat[ARR_SIZE],aTxt[ARR_SIZE];



	if((p_dat = fopen("data.dat","rb"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu data.dat");
		exit(-1);
	}
	fread(aDat, sizeof(aDat[0]), ARR_SIZE, p_dat);	
	fclose(p_dat);



	if((p_txt = fopen("data.txt","r"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu data.txt");
		exit(-1);
	}
	for(int i=0; i < ARR_SIZE; ++i){
		fscanf(p_txt,"%f",&(aTxt[i]));
	}
	fclose(p_txt);

	for (int i=0; i<ARR_SIZE; ++i){
		printf("diff[%d] = %e\n",i,aDat[i] - aTxt[i]);
	}

	puts("\nZmiana co trzeciego elementu");
	if((p_plik = fopen("data.dat","r+b"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu data.dat");
		exit(-1);
	}


	float arr2[ARR_SIZE];
	

	fseek(p_plik,0, SEEK_END);
	long fileLen = ftell(p_plik);
	float zero = 0;


	fseek(p_plik,0, SEEK_SET);
	for (int i=0; i*3*sizeof(float) < fileLen; ++i){
		fwrite( &zero ,sizeof(float),1,p_plik);
		printf("fseek %d\n",fseek(p_plik, 2*sizeof(float), SEEK_CUR));
	}


	fflush(p_plik);
	fseek(p_plik, 0 , SEEK_SET);
	fread(arr2,sizeof(arr2[0]),ARR_SIZE,p_plik);
	printArr(arr2,ARR_SIZE);

	


	return 0;
}

void populateWithRandom(float * arr, int len){
	for (int i=0; i<len; ++i){
		arr[i] = randomFrom(0,100);
	}
}

float randomFrom(float min,float max){
	static time_t date;
	static char singleton; //inicjalizowany jako zero
	
	if (singleton==0){
		singleton = 1;
		srand(time(&date));
	}

	return (rand() / (float)RAND_MAX) * (max - min)  + min;
}


void printArr(float * arr, int len){
	for (int i=0; i<len; ++i){
		printf("arr[%d] = %5.2f\n",i,arr[i]);
	}	
}


long getFileSize(const char * fileName){
	FILE * p_plik;

	if((p_plik = fopen(fileName,"rb"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu %s",fileName);
		exit(-1);
	}

	fseek(p_plik, 0, SEEK_END);
	
	long fileLen = ftell(p_plik);
	
	fclose(p_plik);
	
	return fileLen;

}
