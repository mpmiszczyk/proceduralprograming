#include <stdio.h>

int add(int * a, int * b);
int compare(int * a, int * b);
int compareSort(int * a, int * b);
	
int runWith(int a, int b, int (*func)(int *,int *));

int main (void){
	int a, b;

	printf("prosze podać dwie liczby\n");
	scanf(" %d %d", &a, &b);

	printf("suma: %d\n",runWith(a,b,add));
	printf("porównanie: %d\n",runWith(a,b,compare));
	printf("porównanie przy sortowaniu: %d\n",runWith(a,b,compareSort));
	
	
	

	return 0;
}


int add(int * a, int * b){
	return *a+*b;
}
int compare(int * a, int * b){
	if (*a > *b){
		return 1;
	} else {
		return 0;
	}
}

int compareSort(int * a, int * b){
	return *a-*b;
}

int runWith(int a, int b, int (*func)(int*,int*)){
	return func(&a,&b);
}
