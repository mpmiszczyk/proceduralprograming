/*
 * REKURENCYJNE WYWOŁYWANIE FUNKCJI FIBBONACIEGO
 * WRAZ Z PEŁNYMI OPISAMI DZIAŁAŃ
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct t_alg {
	char * opis;
	long int (*run)(int num);
} Alg;
Alg new_Alg (long int (*run)(int num),char * opis);



typedef struct t_algs {
	int count;
	Alg ** arr_p;
} Algs;
Algs new_Algs ();
void free_Algs (Algs * p_algs);

void addAlg (Alg *alg, Algs *algs);

long int silniaReq (int num);
long int silniaIter (int num);

long int fibbon(int num);
long int fibbonReq(int num, int incepcja, int *p_count);

void HandelError(char * mssg);


int main (void){

	int choice,num;

	Alg alg1 = new_Alg (silniaReq,"silnia licznona sposobem rekurencyjnym\0");
	Alg alg2 = new_Alg (silniaIter,"silnia licznona sposobem iteracujnym\0");
	Alg alg3 = new_Alg (fibbon,"ciąg fibbonacci liczony rekurencyjnie wraz z pełnymi opisami\0");

	Algs algoritms = new_Algs();
	
	addAlg(&alg1, &algoritms );
	addAlg(&alg2, &algoritms );
	addAlg(&alg3, &algoritms );

	do {

		printf ("prosze wybrać algorytm\n");
		for (int i = 0; i < algoritms.count; ++i){
			printf("%d  %s\n",i+1, algoritms.arr_p[i]->opis);
		}
		scanf (" %d",&choice);
		--choice;
	} while (!(0 <= choice && choice < algoritms.count));


	
	printf ("prosze podać liczbę\n");	
	scanf(" %d",&num);
	printf("wynik: %ld\n",algoritms.arr_p[choice]->run(num));


	free_Algs(&algoritms);
		
	
	
	return 0;
}



Alg new_Alg (long int (*run)(int num),char * opis){
	Alg alg;
	alg.opis = opis;
	alg.run = run;

	return alg;

}


Algs new_Algs (){
	Algs algs;
	algs.count = 0;
	algs.arr_p = (Alg**)malloc(sizeof(Alg*));

	return algs;
}
void free_Algs (Algs * p_algs){
	p_algs->count = 0;
	free(p_algs->arr_p);
}


void addAlg (Alg *alg, Algs *algs){
	++algs->count;
	Alg **temp = (Alg**)realloc(algs->arr_p,algs->count * sizeof(Alg*));
	if (temp == NULL){
		--algs->count;
		HandelError("Błąd przypisania pamięci dla Alg w Algs.arr_p");
	} else {
		algs->arr_p = temp;
		algs->arr_p[algs->count-1] = alg;
	}
}


long int silniaReq (int num){
	return num ? ( num * silniaReq(num-1))
				: 1; 
}


long int silniaIter (int num){
	long int silnia = 1;
	for (int k=2; k <= num; ++k){
		silnia *= k;
	}
	return silnia;
}



void HandelError(char * mssg){
	fprintf(stderr,mssg);
}


long int fibbon(int num){
	int count = 0;
	return fibbonReq(num,0,&count);
}

long int fibbonReq(int num, int incepcja, int *p_count){

	++(*p_count);
	for (int i=0; i<incepcja; ++i)
		printf(".");

	if (num <= 1){
		printf("Zwracam F(%d); count = %d\n",num, *p_count);
		return 1;
	} else {
		printf("Obliczam F(%d) = F(%d) + F(%d); count = %d \n",num, num -1 , num -2, *p_count);
		return fibbonReq(num-1, incepcja+1, p_count) + fibbonReq(num - 2, incepcja+1, p_count);
	}
}
