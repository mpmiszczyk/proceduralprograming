/* ptrfn.c */
/* Tablica wskaznikow do funkcji */
#include <stdio.h>
#include <math.h>

double ff(double x){
	return 1+x*x;
}

double anotherFunction(double x){
	printf ("wywolanie innej funkcji z argumentem %lf\n",x);
	return 0;
}
double odwrotnosc(double x){
	return 1/x;
}

int main(void){
	double (*p[])(double) = {sin,cos,ff,anotherFunction,NULL};
	double (**q)(double);

	double arg;

	for (int k=0; k<10; ++k){
		printf("\niteracja nr. : %d\n", k+1);
		printf("proszę podać liczbę\n");
		scanf(" %lf",&arg);

		q=p;
		while(*q) printf("%.4lf\n", (*(*q++))(arg));
	
		
		puts("");
		int i =0;
		while(p[i]){
			printf("%.4lf\n", (*p[i])(arg));
			++i;
		}
		
	}
	return 0;
}

