#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int getMaxLineLen(FILE * p_file);


int main(int argc, char **argv){


	if (argc != 3){
		printf ("Błędny sposób urzycia programu\n");
		printf ("Prosze wywołać w następujący sposób\n");
		printf ("\t%s fileName wordToSearch\n", argv[0]);
		printf ("\n");
		printf ("fileName \t- nazwa pliku w którym są poszukiwane linie tekstu\n");
		printf ("\n");
		printf ("wordToSearch  \t- ciag znaków który będzie poszukiwany w poszczególnych liniach pliku\n");
		exit (-1);
	}

	

	FILE *p_plik;
	char * fileName = argv[1];
	char * word = argv[2];

	printf("otwieranie pliku %s ...\n",argv[1]);
	
	if((p_plik = fopen(fileName,"r"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu %s\n",fileName);
		fprintf(stderr,"Sprawdź czy masz prawa i czy plik istnieje\n");
		exit(-1);
	}




	FILE *p_log;
	char logName[30];
	sprintf(logName, "%s.log",word);
	
	if((p_log = fopen(logName,"w"))==NULL){
		fprintf(stderr,"Błąd otwarcia pliu %s\n",logName);
		exit(-1);
	} else {
		printf("\nPoszukiwane linie będą zapisane w pliku %s\n",logName);
	}
	

	int maxLine = getMaxLineLen(p_plik);
	char *line = malloc((maxLine) * sizeof(char));
	int lineCount = 0;

	while ( fgets (line, maxLine, p_plik)){
		if (strstr(line,word)){
			fputs(line,p_log);
			lineCount ++;
		}
	}

	printf ("Liczba znalezionych lini: %d\n",lineCount);

	fclose(p_log);
	fclose(p_plik);

	return 0;
}



int getMaxLineLen(FILE * p_file){
	long actPosition = ftell(p_file);
	int maxLineLen=0;
	int lineLen=0;


	fseek(p_file, 0 , SEEK_SET);
	
	while(feof(p_file)==0){
		if (fgetc(p_file) != '\n'){
			++lineLen;
		} else {
			if (lineLen > maxLineLen){
				maxLineLen = lineLen;
			}
			lineLen = 0;
		}

	}
	
	
	fseek(p_file, actPosition , SEEK_SET);

	return maxLineLen+1;
	

}
