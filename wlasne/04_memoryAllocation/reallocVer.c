#include <stdio.h>
#include <stdlib.h>


void printArr(const int *arr, unsigned int size);


int main (void){
	
	int *dynamicArray;
	int arrSize;
	int newElement;
	
	printf("podaj wielkość tablicy\n");
	scanf(" %d",&arrSize);

	dynamicArray = (int *)malloc(arrSize * sizeof(dynamicArray[0]));
	
	if (dynamicArray==NULL)	{
		printf ("bład przypisania pamięci\n");
		exit (EXIT_FAILURE);
	}

	for (int i=0; i< arrSize; ++i){
		dynamicArray[i]=i;
	}

	printArr(dynamicArray,arrSize);

	int *newVariableJustForMemoryAllocatoin;

	newVariableJustForMemoryAllocatoin = (int *)malloc ( 3*sizeof(int));

	printf("adres tablicy: %p\n",dynamicArray);
	printf("podaj element do dodania\n");
	scanf(" %d", &newElement);

	arrSize *=3;
	dynamicArray = realloc(dynamicArray,arrSize* sizeof(dynamicArray[0]));
	if (dynamicArray==NULL)	{
		printf ("bład przypisania pamięci\n");
		exit (EXIT_FAILURE);
	}
	dynamicArray[arrSize-1] = newElement; 
	

	printf("\npo dodaniu\n");
	printArr(dynamicArray,arrSize);

	printf("adres po dodaniu: %p\n",dynamicArray);
	

	free(dynamicArray);
	free(newVariableJustForMemoryAllocatoin);

	return 0;
}


void printArr(const int * arr, unsigned int size){

	for (int i=0; i<size; i++){
		printf("arr[%2d] = %d\n", i, arr[i]);
	}

}
