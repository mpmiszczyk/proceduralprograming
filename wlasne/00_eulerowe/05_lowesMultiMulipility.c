#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define false 0
#define true 1



int getPrimeNumber(int count);
int findPrimeGreaterThen (int *lastCheck ,const int* primes,const int *primeCout);

struct PrimeFactor {
	int value;
	int count;
};
struct PrimeFactors {
	int count;
	struct PrimeFactor *arr;
};

struct PrimeFactors getPrimeFactorsOf(int number);
void addPrime(int prime, struct PrimeFactors *factors);

void compareAndAdd (struct PrimeFactors *total, struct PrimeFactors ofNumber);

int createNumberFromFactors(const struct PrimeFactors factors);





int main (int argc, char *argv[]){
	puts(argv[0]);
	puts("\n");

	struct PrimeFactors totalPrimeFactors;
	totalPrimeFactors.count = 0;
	totalPrimeFactors.arr = (struct PrimeFactor *)malloc(sizeof (struct PrimeFactor));

	int maxNumber =20;


	for ( int i = 2; i<= maxNumber; ++i){
		struct PrimeFactors primeFactors;
		primeFactors = getPrimeFactorsOf (i);
		compareAndAdd( &totalPrimeFactors, primeFactors);		
	}
	

	printf( "wynik %d\n",createNumberFromFactors(totalPrimeFactors));
	
	

	return 0;
}






int getPrimeNumber(int count){
	static int primeCout = 0;
	static int *primes; //TODO jak free te pierwsze ??, czy może statycznych nie trzeba??
	static int lastCheck = 1;
	
	static int singleton = false;
		
	if (singleton == false){
		singleton = true;
		primes = (int*)malloc(sizeof(int));
	}


	int prime;

	while (primeCout < count){
		prime = findPrimeGreaterThen (&lastCheck , primes, &primeCout);
		++primeCout;
		int * newPrimes = realloc(primes, primeCout * sizeof (primes[0]));
		if (newPrimes == NULL){
			fprintf(stderr , "!!Blad przypisania pamięci do tablicy o rozmiarze %d", primeCout);
			free (primes);
			exit(-1);
		} 

		primes = newPrimes;
		primes[primeCout - 1] = prime;
	}
	
	prime = primes[count - 1];
	return prime;

}

int findPrimeGreaterThen (int *lastCheck ,const int* primes,const int *primeCout){

	int prime = 0;

	while (prime == 0){
		++(*lastCheck);
		int isPrime = true;

		for (int i=0; i<*primeCout; ++i){
			if ( *lastCheck % primes[i] == 0 ){
				isPrime = false;
				break;
			}	
		}

		if (isPrime) {
			prime = *lastCheck;
		}
	}
	

	return prime;
	
}



struct PrimeFactors getPrimeFactorsOf(int number){
	struct PrimeFactors factors;
	factors.count = 0;
	factors.arr = (struct PrimeFactor *)malloc(sizeof (struct PrimeFactor));
	
	int primeNumber = 1;


	while (number !=1){
		int prime = getPrimeNumber(primeNumber);
		if (number % prime == 0){
			number /= prime;
			addPrime(prime , &factors);
		} else {
			++primeNumber;
		}
	}

	return factors;		
}

void addPrime(int prime, struct PrimeFactors *factors){
	//printf("addPrime ");
	for (int i =0; i< (*factors).count; ++i){
		if ((*factors).arr[i].value == prime){
			++(*factors).arr[i].count;
			return;
		}
	} //else 
	++(*factors).count;
	(*factors).arr = (struct PrimeFactor*)realloc((*factors).arr , sizeof (struct PrimeFactor) * (*factors).count);
	(*factors).arr[(*factors).count-1].value = prime;
	(*factors).arr[(*factors).count-1].count = 1;
	
}

void compareAndAdd (struct PrimeFactors *total, struct PrimeFactors ofNumber){

	for (int i = 0; i < ofNumber.count; ++i){
	int k;
	for ( k = 0; k < (*total).count; ++k){
		if ( ofNumber.arr[i].value == (*total).arr[k].value){
			if ( ofNumber.arr[i].count > (*total).arr[k].count){
				(*total).arr[k].count = ofNumber.arr[i].count;
			}
			
			break;
		} 
		
	}
	if ( k == (*total).count){ //nie znalazł pierwszej w total
		for (int l=0; l<ofNumber.arr[i].count; ++l){
			addPrime(ofNumber.arr[i].value, total);
		}
	}
	}
}


int createNumberFromFactors(const struct PrimeFactors factors){
	int multiply = 1;

	for (int i=0; i<factors.count; ++i){
	for (int k=0; k<factors.arr[i].count; ++k){
		multiply *= factors.arr[i].value;
	}
	}

	return multiply;
}

