#include <stdio.h>
#define true 1
#define false 0


int isPalidrome(int number);
int countDigits(int number);


int main (int argc, char *argv[]){

	puts(argv[0]);

	printf("%d isPalidrome %d\n", 1, isPalidrome(1));
	printf("%d isPalidrome %d\n", 31, isPalidrome(31));
	printf("%d isPalidrome %d\n", 13, isPalidrome(13));
	printf("%d isPalidrome %d\n", 212, isPalidrome(212));
	printf("%d isPalidrome %d\n", 2002, isPalidrome(2002));

	int greatest =0;	
	int max = 999;
	int min = 99;

	for (int i = max; i>min; --i){
		if ( i*max <= greatest){
			break;
		}		
		for (int j = max; j>=i ; --j ){
			int number = i*j;
			if (number < greatest){
				break;
			}
			if (isPalidrome(number) && number > greatest){
				greatest = number;
			}
		}
	}
	
	printf ("greatest %d\n",greatest);


	return 0;
}



int isPalidrome(int number){
	
	int length = countDigits(number);
	int digits[length];

	for (int i =0; i< length; ++i){
		digits[i] = number % 10;
		number /= 10;
	}

	for (int i =0; i< length/2; ++i){
		if (digits[i] != digits[length-1 - i]){
			return false;
		}
	}
	return true;
}

int countDigits(int number){
	int count =0;

	while (number !=0){
		number /= 10;
		++count;
	}

	return count;
}
