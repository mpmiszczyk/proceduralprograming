#include <stdio.h>
#include <stdlib.h>
#define false 0
#define true 1



int getPrimeNumber(int count);
int findPrimeGreaterThen (int *lastCheck ,const int* primes,const int *primeCout);


int main (int argc, char *argv[]){
	puts(argv[0]);
	puts("\n");

	
	long unsigned  int number;
	unsigned int primeNumber=1;

	do {
		printf ("prosze podać liczbe do rozlorzenia na czynniki pierwsze\n");
	} while ( scanf(" %lu",&number) == EOF);

	
	
	while (number != 1){
		int prime = getPrimeNumber(primeNumber);
		if ( number % prime == 0){
			number /= prime;
			printf ("prime = %d \n",prime);
		}  else {
			++primeNumber;
		}
	}

	return 0;
}

int getPrimeNumber(int count){
	static int primeCout = 0;
	static int *primes; //TODO jak free te pierwsze ??, czy może statycznych nie trzeba??
	static int lastCheck = 1;
	
	static int singleton = false;
		
	if (singleton == false){
		singleton = true;
		primes = (int*)malloc(sizeof(int));
	}


	int prime;

	while (primeCout < count){
		prime = findPrimeGreaterThen (&lastCheck , primes, &primeCout);
		++primeCout;
		int * newPrimes = realloc(primes, primeCout * sizeof (primes[0]));
		if (newPrimes == NULL){
			fprintf(stderr , "!!Blad przypisania pamięci do tablicy o rozmiarze %d", primeCout);
			free (primes);
			exit(-1);
		} 

		primes = newPrimes;
		primes[primeCout - 1] = prime;
	}
	
	prime = primes[count - 1];
	return prime;

}

int findPrimeGreaterThen (int *lastCheck ,const int* primes,const int *primeCout){

	int prime = 0;

	while (prime == 0){
		++(*lastCheck);
		int isPrime = true;

		for (int i=0; i<*primeCout; ++i){
			if ( *lastCheck % primes[i] == 0 ){
				isPrime = false;
				break;
			}	
		}

		if (isPrime) {
			prime = *lastCheck;
		}
	}
	

	return prime;
	
}
