#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#define false 0
#define true 1



unsigned int getPrimeNumber(unsigned int count);
unsigned int findPrimeGreaterThen (unsigned long int *lastCheck ,
				const unsigned int* primes,
				const unsigned int *primeCout);

struct PrimeFactor {
	int value;
	int count;
};
struct PrimeFactors {
	int count;
	struct PrimeFactor *arr;
};

struct PrimeFactors new_PrimeFactors(){

	struct PrimeFactors primeFactors;
	primeFactors.count = 0;
	primeFactors.arr = (struct PrimeFactor *)malloc(sizeof (struct PrimeFactor));

	return primeFactors;
	
}
struct PrimeFactors getPrimeFactorsOf(unsigned long int number);
void addPrime(unsigned int prime, struct PrimeFactors *factors);
int createNumberFromFactors(const struct PrimeFactors factors);
void printFactors(const struct PrimeFactors *factors);
int countDivisiors(const struct PrimeFactors *factors);
int countAllPairs(const struct PrimeFactors *factors);
int countUniquePairs(const struct PrimeFactors *factors);




int main (int argc, char *argv[]){
	puts(argv[0]);
	puts("\n");

	time_t start, stop;

	double tstart, tstop;

	time(&start);
	tstart = (double)clock()/CLOCKS_PER_SEC;

	unsigned long int sum;

	unsigned long int smallerThen = pow (10 , 6);
	printf("smallerThen %lu\n",smallerThen);


	for (unsigned long int i = 1; i<=smallerThen; ++i){
		//printf ("%.0f \n",i);
		struct PrimeFactors factors = getPrimeFactorsOf(i);
		sum += countUniquePairs(&factors);
	}

	printf("sum %lu\n",sum);	


	time(&stop);
	tstop = (double)clock()/CLOCKS_PER_SEC;

	printf("Finished in about %.3f seconds. \n", difftime(stop, start));
	printf("Finished in about %.3f seconds. \n", tstop-tstart);


	return 0;
}






unsigned int getPrimeNumber(unsigned int count){
	static unsigned int primeCout = 0;
	static unsigned int *primes; //TODO jak free te pierwsze ??, czy może statycznych nie trzeba??
	static unsigned long int lastCheck = 1;
	
	static int singleton = false;
		
	if (singleton == false){
		singleton = true;
		primes = (int*)malloc(sizeof(int));
	}


	unsigned int prime;

	while (primeCout < count){
		prime = findPrimeGreaterThen (&lastCheck , primes, &primeCout);
		++primeCout;
		int * newPrimes = realloc(primes, primeCout * sizeof (primes[0]));
		if (newPrimes == NULL){
			fprintf(stderr , "!!Blad przypisania pamięci do tablicy o rozmiarze %d", primeCout);
			free (primes);
			exit(-1);
		} 

		primes = newPrimes;
		primes[primeCout - 1] = prime;
	}
	
	prime = primes[count - 1];
	return prime;

}

unsigned int findPrimeGreaterThen (unsigned long int *lastCheck ,
				const unsigned int* primes,
				const unsigned int *primeCout){

	unsigned int prime = 0;

	while (prime == 0){
		++(*lastCheck);
		int isPrime = true;

		for (unsigned int i=0; i<*primeCout; ++i){
			if ( *lastCheck % primes[i] == 0 ){
				isPrime = false;
				break;
			}	
		}

		if (isPrime) {
			prime = *lastCheck;
		}
	}
	

	return prime;
	
}



struct PrimeFactors getPrimeFactorsOf(unsigned long int number){
	struct PrimeFactors factors = new_PrimeFactors();

	int primeNumber = 1;


	while (number !=1){
		int prime = getPrimeNumber(primeNumber);
		if (number % prime == 0){
			number /= prime;
			addPrime(prime , &factors);
		} else {
			++primeNumber;
		}
	}

	return factors;		
}

void addPrime(unsigned int prime, struct PrimeFactors *factors){
	//printf("addPrime ");
	for (unsigned int i =0; i< (*factors).count; ++i){
		if ((*factors).arr[i].value == prime){
			++(*factors).arr[i].count;
			return;
		}
	} //else 
	++(*factors).count;
	(*factors).arr = (struct PrimeFactor*)realloc((*factors).arr , sizeof (struct PrimeFactor) * (*factors).count);
	(*factors).arr[(*factors).count-1].value = prime;
	(*factors).arr[(*factors).count-1].count = 1;
	
}


int createNumberFromFactors(const struct PrimeFactors factors){
	int multiply = 1;

	for (int i=0; i<factors.count; ++i){
	for (int k=0; k<factors.arr[i].count; ++k){
		multiply *= factors.arr[i].value;
	}
	}

	return multiply;
}

void compareAndAdd (struct PrimeFactors *total, struct PrimeFactors ofNumber){

	for (int i = 0; i < ofNumber.count; ++i){
	int k;
	for ( k = 0; k < (*total).count; ++k){
		if ( ofNumber.arr[i].value == (*total).arr[k].value){
			if ( ofNumber.arr[i].count > (*total).arr[k].count){
				(*total).arr[k].count = ofNumber.arr[i].count;
			}
			
			break;
		} 
		
	}
	if ( k == (*total).count){ //nie znalazł pierwszej w total
		for (int l=0; l<ofNumber.arr[i].count; ++l){
			addPrime(ofNumber.arr[i].value, total);
		}
	}
	}
}

struct PrimeFactors sumFactors (struct PrimeFactors a, struct PrimeFactors b){

	struct PrimeFactors sumFactors = new_PrimeFactors();
	
	compareAndAdd(&sumFactors , a);
	compareAndAdd(&sumFactors , b);

}


void printFactors(const struct PrimeFactors *factors){
	printf("%d = ", createNumberFromFactors(*factors));
	for (int i=0; i<(*factors).count; ++i){	
	for (int k=0; k<(*factors).arr[i].count; ++k){
		printf("%d ",(*factors).arr[i].value);
	}
	}
	printf("\n");
}


int countDivisiors(const struct PrimeFactors *factors){
	
	int sum = 1; //? czy 1?

	for (int i=0; i<(*factors).count; ++i){	
		int primeCout = (*factors).arr[i].count;
		sum *= primeCout+ 1;
	}

	return sum;	
}


int countAllPairs(const struct PrimeFactors *factors){
	
	int sum = 1; //? czy 1?

	for (int i=0; i<(*factors).count; ++i){	
		int primeCout = (*factors).arr[i].count;
		sum *= primeCout + primeCout + 1;
	}
	
	++sum;

	return sum;
}

int countUniquePairs(const struct PrimeFactors *factors){
	return countAllPairs(factors) / 2;
}

