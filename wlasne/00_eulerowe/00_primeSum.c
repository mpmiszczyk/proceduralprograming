#include <stdio.h>
#define PRIME_MAX 2000001
#define true 1
#define false 0

int main (void){
	int primeNumber;

	printf ("prosze limit liczbe pierszych do zsumowania, ");
	do {
		printf ("liczba musi byc mniejsza %d\n",PRIME_MAX);
		scanf (" %d",&primeNumber);

	}while ( primeNumber > PRIME_MAX);

	int primes[primeNumber];
	int primeCount = 1;

	primes[0] = 2;

	long long int sum = 2;
	

	for ( int i=3 ; i <= primeNumber; ++i  ){
		char isPrime = true;
		for (int k = 0; k<primeCount; ++k){
			if ( i % primes[k] == 0 ){
				isPrime = false;
				break;
			}
		}
		if (isPrime){
			primes[primeCount]=i;
			primeCount++;
	
			sum += i;
			
			printf("prime[%5d] = %13d\n",primeCount-1,i);
		}
	}

	printf ("sum = %ld\n",sum);
	

	return 0;
}
