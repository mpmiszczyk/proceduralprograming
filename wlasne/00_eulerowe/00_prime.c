#include <stdio.h>
#define PRIME_MAX 1000000
#define true 1
#define false 0

int main (void){
	int primeNumber = 10001;

	printf ("prosze podac liczbe pierszych do znalezienia, ");
	do {
		printf ("liczba musi byc mniejsza %d\n",PRIME_MAX);
		scanf (" %d",&primeNumber);

	}while ( primeNumber > PRIME_MAX);

	int primes[primeNumber];
	int primeCount = 1;

	primes[0] = 2;

	

	for ( int i=3 ; primeCount < primeNumber; ++i  ){
		char isPrime = true;
		for (int k = 0; k<primeCount; ++k){
			if ( i % primes[k] == 0 ){
				isPrime = false;
				break;
			}
		}
		if (isPrime){
			primes[primeCount]=i;
			primeCount++;
			
			printf("prime[%5d] = %13d\n",primeCount-1,i);
		}
	}
	

	return 0;
}
