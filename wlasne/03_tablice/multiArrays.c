#include <stdio.h>


//void printArrAdresys( int arr[][]); !!!BŁAD KOMPILACJI
int size = 1;
void printArrAdresys( int arr[][size]);

int main (void){

	int row;
	int kolumn;

	printf("prosze podać ilość kolumn i wierszy \n");
	scanf(" %d %d",&row,&kolumn);

	

	int arr[row][kolumn];

	size = kolumn;

	printArrAdresys(arr);



	/**
	*tablica rezerwuje tylko i wyłącznie pamięć na dane;
	*bez osobnej pamięci na wskaźniki
	*jak to dzieje się w przypadku tablic deklarowanych na wskaźnikach
	*a same poddablice to po prostu typy z nadpisaną wielkością inkrementacji
	*/
	
	/**
	* nie jest to tak intuicyjne jak bym się spodziewał
	* ale jest to jak najbardziej do zrozumienia
	*/

	return 0;
}


void printArrAdresys( int arr[][size]){

	printf("adres pod zmienna tablicy  :  *adr         = %p\n", arr);
	printf("adres pierwszej podtablicy :  &(arr[0])    = %p\n", &(arr[0]));
	printf("adres pod zmienna tablicy  :  &(arr[0][0]) = %p\n", &(arr[0][0]));


	printf("\n");
	printf("adres pierwszej podtablicy :   arr + 1     = %p\n", (arr+1));
	printf("adres pierwszej podtablicy :  &(arr[1])    = %p\n", &(arr[1]));
	printf("adres pod zmienna tablicy  :  &(arr[1][0]) = %p\n", &(arr[1][0]));

}
