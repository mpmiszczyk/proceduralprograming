#include <stdio.h>

void someFuction(int tab[]);
void changeArrayStart(int tab[]);
void changeArrayStart2(int *tab[]);


int main (void){

	
	int tab[100];
	
	int tabSize = sizeof (tab) / sizeof (tab[0]);
	//zwraca dlugosc tablicy; instrukcja prekompilatora
	
	printf ("wielkosc tablicy: %d\n",tabSize);

	someFuction(tab); //bledna dlugosc wewnatrz funkcji

	/* C99 */
	
	int z = 9;
	
	int tab_99[z];
	int tabSize_99 = sizeof (tab_99) / sizeof (tab_99[0]);
	printf ("wielkosc tablicy 99: %d\n",tabSize_99); //poprawne wartosci
	

	printf("\n podaj dlugosc tablicy\n");
	scanf(" %d", &z);
	
	int tab_99_b[z];
	int tabSize_99_b = sizeof (tab_99_b) / sizeof (tab_99_b[0]);
	printf ("wielkosc tablicy 99_b: %d\n",tabSize_99_b); //poprawne wartosci

	z = 0;
	int tabSize_99_c = sizeof (tab_99_b) / sizeof (tab_99_b[0]);
	printf ("wielkosc tablicy 99_c: %d\n",tabSize_99_c); //wciaz poprawne wartosci
	
	someFuction(tab_99_b); //bledna dlugosc wewnatrz funkcji

	printf("\n podaj dlugosc kolejnej tablicy\n");
	scanf(" %d", &z);
	
	int tab_99_d[z];
	int tabSize_99_d = sizeof (tab_99_d) / sizeof (tab_99_d[0]);
	printf ("wielkosc tablicy 99_d: %d\n",tabSize_99_d); //poprawne wartosci

	printf ("zmiana wskaznika nowej tablicy\n");
	tab_99_d[0]=0;
	tab_99_d[1]=1;
	changeArrayStart(tab_99_d); //brak mozliwosci zmiany wartosci wskaznika tablicy (stala)
	printf("wartosc w zerowej komurce po zamianie: %d\n",tab_99_d[0]);
	tabSize_99_d = sizeof (tab_99_d) / sizeof (tab_99_d[0]);
	printf ("wielkosc tablicy 99_d: %d\n",tabSize_99_d); //poprawne wartosci
	


	printf ("inna zmiana wskaznika nowej tablicy\n");\
	tab_99_d[0]=0;
	tab_99_d[1]=1;
	changeArrayStart2(&tab_99_d); //dokonuje zmiany, ale nie w sposub jaki był by oczekiwany; +warningi przy kompilacji; 
	printf("wartosc w zerowej komurce po zamianie: %d\n",tab_99_d[0]);
	tabSize_99_d = sizeof (tab_99_d) / sizeof (tab_99_d[0]);
	printf ("wielkosc tablicy 99_d: %d\n",tabSize_99_d); //poprawne wartosci

	



	return 0;

	
	
}



void someFuction(int tab[]){
	
	int tabSize = sizeof (tab) / sizeof (tab[0]);
	//zawsze zwroci tylko i wylacznie 1, bo tab to wskaznik do pierwszego elementu
	//!!!!!!!chociarz mi zupelnie nielogicznie zwrocilo 2 !?!?!?!?!?!? WAT !?!?!?!?!?!?

	
	printf ("\nw funkcji\n");
	printf ("wielkosc tablicy: %d\n",tabSize);
}


void changeArrayStart(int *tab){
	
	++tab; //zwiększa się wartość która znajduje się na stosie
}
void changeArrayStart2(int *tab[]){
	
	++(*tab); 
}
